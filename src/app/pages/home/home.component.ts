import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AppService } from '../../app.service';
import { Product } from "../../app.models";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public slides = [
    { title: 'End of Season Sale', subtitle: 'Special for today', image: 'assets/images/carousel/banner1.jpg' },
    { title: 'End of Season Sale', subtitle: 'New Arrivals On Sale', image: 'assets/images/carousel/banner2.jpg' },
    { title: 'End of Season Sale', subtitle: 'Special for today', image: 'assets/images/carousel/banner3.jpg' },
    { title: 'End of Season Sale', subtitle: 'New Arrivals On Sale', image: 'assets/images/carousel/banner4.jpg' },
    { title: 'End of Season Sale', subtitle: 'Special for today', image: 'assets/images/carousel/banner5.jpg' }
  ];

  public brands = [];
  public banners = [];
  public featuredProducts: Array<Product>;
  public onSaleProducts: Array<Product>;
  public topRatedProducts: Array<Product>;
  public newArrivalsProducts: Array<Product>;
  public product: any;


  constructor(public appService: AppService, public changeDetectRef: ChangeDetectorRef) { }

  ngOnInit() {
    
    this.mainProduct();
  }

  public onLinkClick(e) {
    
  }
  public mainProduct() {
    return this.appService.getMainProducts().then((data) => {
    });
  }

}
