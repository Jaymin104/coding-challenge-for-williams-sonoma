import { SidenavMenu } from './sidenav-menu.model';

export const sidenavMenuItems = [ 
    new SidenavMenu (1, 'Home', '/', null, null, false, 0),
    new SidenavMenu (1, 'Furniture', '/', null, null, false, 0),
    new SidenavMenu (1, 'Outdoor', '/', null, null, false, 0),
    new SidenavMenu (1, 'Rugs', '/', null, null, false, 0),
    new SidenavMenu (1, 'Bedding', '/', null, null, false, 0),
    new SidenavMenu (1, 'Bath', '/', null, null, false, 0)
]