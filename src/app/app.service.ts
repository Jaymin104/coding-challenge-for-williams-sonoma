import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { MatSnackBar } from '@angular/material';
import { Category, Product } from './app.models';

export class Data {
    constructor(public categories: Category[],
        public compareList: Product[],
        public wishList: Product[],
        public cartList: Product[],
        public totalPrice: number) { }
}

@Injectable()
export class AppService {
    public mainProductResponse: any;
    public Data = new Data(
        [], // categories
        [], // compareList
        [],  // wishList
        [],  // cartList
        null //totalPrice
    )
    /**
 * Performs HTTP POST Request on given API End Point with given POST data
 * @param apiEndPoint API End Point to perform POST request on
 * @param postObj POST Data to be sent in request
 */
    //   httpPost(apiEndPoint: string, postObj: Object) {
    //     return new Promise((resolve, reject) => {
    //       this.http.post(this.API_URL + apiEndPoint, postObj)
    //         .subscribe(res => {
    //           this.tempData = res;
    //           if (this.tempData.success && this.tempData.data != null) {
    //             resolve(this.tempData.data);
    //           } else {
    //             reject(this.tempData.message);
    //           }
    //         }, err => {
    //           console.error(err);
    //           reject(err);
    //         });
    //     });
    //   }

    public url = "assets/data/";
    public main_url = "assets/json/response.json";
    constructor(public http: HttpClient, public snackBar: MatSnackBar) { }
    public getMainProducts() {
        return new Promise((resolve, reject) => {
            this.http.get(this.main_url)
                .subscribe(res => {
                    this.mainProductResponse = res;
                    console.log('Result', res);
                    if (this.mainProductResponse != null) {
                        resolve(this.mainProductResponse);
                    } else {
                        reject(this.mainProductResponse);
                    }
                }, err => {
                    console.error(err);
                    reject(err);
                });
        });
        // this.http.get(this.main_url).subscribe((res: any) => {
        //     console.log(res);
        //     this.mainProductResponse = res;
        // });
    }
} 