# Coding Challenge for Williams Sonoma #

This project is a demonstration of my skills with Angular, Responsive Experience, and UI/UX Design.

# Installation #
Prerequisites
Version Control: git
node npm
angular cli

# Installing #
Download this project to your computer by either clicking 'Download ZIP' above then extracting the archive to your prefered folder.

## Or clone it if you have git installed: ##

## Run the Site Locally ##
With npm installed, open your command line navigate to the project folder:

cd path/to/project/directory
Once the command line is looking at that directory, run following commands:
First run npm install 
then
ng serve --o

If the server is running successfully, you should see the output in browser

Once the server is up and running, open your browser of choice and navigate to:

http://localhost:4200/

The site should load and you can play with the UI.

# Running the tests #
Unit Tests
This project is tested with custom unit tests.

To run the tests, make sure the server is running and navigate your project in cli:

ng test
